#!/bin/sh

python manage.py collectstatic --noinput&&python manage.py makemigrations&&python manage.py migrate&&/usr/local/bin/gunicorn wedding_backend.wsgi:application --reload -w 2 -b :8000 --timeout 600
