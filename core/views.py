from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from ipware import get_client_ip

from core.models import RSVP


def get_client_ip_address(request):
    ip, is_routable = get_client_ip(request)
    if ip is None:
        return -1
    else:
        return ip

def index(request):
    return render(request,'index.html')

def login_user(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        pwd = request.POST.get('password')
        user = authenticate(request,username=username,password=pwd)
        if user:
            login(request,user)
            return redirect("view-rsvp")
        else:
            messages.error(request,'Invalid Credentials')

    return render(request,'admin_login.html')

def rsvp(request):
    ip = get_client_ip_address(request)
    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        #number_of_guests = request.POST.get('number_of_guests')
        msg = request.POST.get('msg')
        try:
            RSVP.objects.create(name=name,email=email,number_of_guests=1,msg=msg,ip=ip)
            messages.success(request,' ')
        except Exception as e:
            messages.error(request,'Sorry,Something went Wrong.Try Again')
            print(e)
    return redirect('home')

@login_required(login_url='/login')
def view_rsvp(request):
    rsvp_objects = RSVP.objects.all()
    return render(request,'view_rsvp.html',{'rsvp_objs':rsvp_objects})