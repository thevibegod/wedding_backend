from django.db import models
from django.utils import timezone as tz

class RSVP(models.Model):
    name = models.CharField(max_length=120)
    email = models.EmailField()
    number_of_guests = models.PositiveSmallIntegerField(default=1)
    msg = models.TextField(null=True,blank=True)
    ip = models.CharField(max_length=30)
    timestamp = models.DateTimeField(default=tz.now)
